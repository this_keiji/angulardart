import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:angulardart/src/route_paths.dart';
import 'package:angulardart/src/routes.dart';
import 'package:angulardart/src/services/hero_service.dart';

// AngularDart info: https://webdev.dartlang.org/angular
// Components info: https://webdev.dartlang.org/components

@Component(
    selector: 'my-app',
    styleUrls: ['app_component.css'],
    templateUrl: 'app_component.html',
    directives: [routerDirectives],
    providers: [ClassProvider(HeroService)],
    exports: [RoutePaths, Routes],
)
class AppComponent {
    final title = 'Tour of Heroes';
}
