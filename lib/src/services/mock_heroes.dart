import 'package:angulardart/src/models/hero.dart';

final mockHeroes = <Hero>[
  Hero(1, 'Mr. Nice'),
  Hero(2, 'Narco'),
  Hero(3, 'Bombasto'),
  Hero(4, 'Celeritas'),
  Hero(5, 'Magneta'),
  Hero(6, 'RubberMan'),
  Hero(7, 'Dynama'),
  Hero(8, 'Dr IQ'),
  Hero(9, 'Magma'),
  Hero(10, 'Tornado')
];
