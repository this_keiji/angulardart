import 'package:angular_router/angular_router.dart';
import 'package:angulardart/src/route_paths.dart';
import 'components/hero-detail/hero_detail_component.template.dart' as hero_detail_template;
import 'components/hero/hero_component.template.dart' as hero_template;
import 'components/dashboard/dashboard_component.template.dart' as dashboard_template;

export 'route_paths.dart';

class Routes {
    static final hero = RouteDefinition(
        routePath: RoutePaths.hero,
        component: hero_detail_template.HeroDetailComponentNgFactory,
    );

    static final heroes = RouteDefinition(
        routePath: RoutePaths.heroes,
        component: hero_template.HeroComponentNgFactory,
    );

    static final dashboard = RouteDefinition(
        routePath: RoutePaths.dashboard,
        component: dashboard_template.DashboardComponentNgFactory,
    );

    static final all = <RouteDefinition>[
        hero,
        heroes,
        dashboard,
        RouteDefinition.redirect(
            path: '',
            redirectTo: RoutePaths.dashboard.toUrl(),
        )
    ];
}