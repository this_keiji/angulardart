import 'package:angular/angular.dart';
import 'package:angular_forms/angular_forms.dart';
import 'package:angulardart/src/models/hero.dart';
import 'package:angulardart/src/services/hero_service.dart';
import 'package:angular_router/angular_router.dart';
import '../../route_paths.dart';

@Component(
    selector: 'hero-detail-component',
    templateUrl: 'hero_detail_component.html',
    styleUrls: ['hero_detail_component.css'],
    directives: [coreDirectives, formDirectives])
class HeroDetailComponent implements OnActivate {
    final HeroService _heroService;
    final Location _location;
    Hero hero;

    HeroDetailComponent(this._heroService, this._location);

    @override
    void onActivate(_, RouterState current) async {
        final id = getId(current.parameters);
        if (id != null) hero = await (_heroService.get(id));
    }

    int getId(Map<String, String> parameters) {
        final id = parameters[idParam];
        return id == null ? null : int.tryParse(id);
    }

    void goBack() => _location.back();

    Future<void> save() async {
        await _heroService.update(hero);
        goBack();
    }
}
