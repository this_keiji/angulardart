import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:angulardart/src/components/hero-search/hero_search_component.dart';
import 'package:angulardart/src/models/hero.dart';
import 'package:angulardart/src/services/hero_service.dart';

import '../../route_paths.dart';

@Component(
    selector: 'dashboard',
    templateUrl: 'dashboard_component.html',
    styleUrls: ['dashboard_component.css'],
    directives: [coreDirectives, routerDirectives, HeroSearchComponent],
)
class DashboardComponent implements OnInit{
    final HeroService _heroService;

    List<Hero> heroes;

    DashboardComponent(this._heroService);

    @override
    void ngOnInit() async {
        heroes = (await this._heroService.getAllSlowly())
            .skip(1)
            .take(4)
            .toList();
    }

    String heroUrl(int id) => RoutePaths.hero.toUrl(parameters: {idParam: '$id'});
}
