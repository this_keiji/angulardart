import 'package:angular/angular.dart';
import 'package:angular_forms/angular_forms.dart';
import 'package:angular_router/angular_router.dart';
import 'package:angulardart/src/models/hero.dart';
import 'package:angulardart/src/services/hero_service.dart';

import '../../route_paths.dart';

@Component(
    selector: 'hero-component',
    templateUrl: 'hero_component.html',
    styleUrls: ['hero_component.css'],
    directives: [coreDirectives, formDirectives],
    pipes: [commonPipes],
)
class HeroComponent implements OnInit {
    final HeroService _heroService;
    final Router _router;

    List<Hero> heroes;
    Hero selected;

    HeroComponent(this._heroService, this._router);

    @override
    void ngOnInit() => this._getHeroes();

    void onSelect(Hero hero) => selected = hero;

    // without 'await'
//    void _getHeroes() {
//        _heroService.getAll().then((heroes) => this.heroes = heroes);
//    }

    // with 'await'
    Future<void> _getHeroes() async {
        heroes = await _heroService.getAllSlowly();
    }

    String _heroUrl(int id) =>
        RoutePaths.hero.toUrl(parameters: {idParam: '$id'});

    Future<NavigationResult> gotoDetail() =>
        _router.navigate(_heroUrl(selected.id));

    Future<void> add(String name) async {
        name = name.trim();
        if (name.isEmpty) return null;
        heroes.add(await _heroService.create(name));
        selected = null;
    }

    Future<void> delete(Hero hero) async {
        await _heroService.delete(hero.id);
        heroes.remove(hero);
        if (selected == hero) selected = null;
    }
}
